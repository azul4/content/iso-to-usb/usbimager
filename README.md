# usbimager

Minimal GUI application to write compressed disk images to USB drives (GTK+ Frontend)

https://gitlab.com/bztsrc/usbimager

<br><br>
How to clone this repository:

```
git clone https://gitlab.com/azul4/content/iso-to-usb/usbimager.git
```
